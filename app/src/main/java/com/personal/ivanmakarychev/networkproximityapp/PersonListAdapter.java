package com.personal.ivanmakarychev.networkproximityapp;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.personal.ivanmakarychev.networkproximityapp.model.Person;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PersonListAdapter extends RecyclerView.Adapter<PersonListAdapter.PersonViewHolder> {

    private static final String TAG = "PersonListAdapter";
    private static final int PERSON_CARD = 0;
    private static final int FOOTER = 1;

    private final PersonHolderList personHolderList;

    public PersonListAdapter(PersonHolderList dataset) {
        personHolderList = dataset;
    }

    public Person getPerson(int position) {
        return personHolderList.getPerson(position);
    }

    @NonNull
    @Override
    public PersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        switch (i) {
            case PERSON_CARD:
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.person_card_layout, parent, false);
                return new PersonCardViewHolder(v);
            default:
                return new PersonListFooterViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.person_list_footer, parent, false));

        }
    }

    @Override
    public int getItemViewType(int position) {
        return personHolderList.getHolderType(position);
    }

    @Override
    public void onBindViewHolder(@NonNull PersonViewHolder holder, int i) {
        holder.bindTo(getPerson(i));
    }

    @Override
    public int getItemCount() {
        return personHolderList.size();
    }

    static abstract class PersonViewHolder extends RecyclerView.ViewHolder {
        PersonViewHolder(View itemView) {
            super(itemView);
        }

        void bindTo(final Person item) {
            // empty
        }
    }

    static class PersonCardViewHolder extends PersonViewHolder {
        TextView textViewName;
        TextView textViewJob;
        ImageView imageView;

        PersonCardViewHolder(View itemView) {
            super(itemView);
            this.textViewName = (TextView) itemView.findViewById(R.id.textViewName);
            this.textViewJob = (TextView) itemView.findViewById(R.id.textViewJob);
            this.imageView = (ImageView) itemView.findViewById(R.id.imageView);
        }

        @Override
        void bindTo(final Person item) {
            textViewName.setText(item.getName());
            textViewJob.setText(item.getJob());
            Picasso.with(imageView.getContext())
                    .load(item.getImageUrl())
                    .placeholder(R.drawable.loading_indicator)
                    .error(R.drawable.camera_200)
                    .into(imageView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getUrl()));
                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }

    static class PersonListFooterViewHolder extends PersonViewHolder {
        PersonListFooterViewHolder(View view) {
            super(view);
        }
    }

    static class PersonHolder {

        private final Person person;

        PersonHolder(Person person) {
            this.person = person;
        }

        boolean isFooter() {
            return person == null;
        }

        Person getPerson() {
            return person;
        }
    }

    static class PersonHolderList {

        private final List<PersonHolder> personHolderList = new ArrayList<>();

        public int addPerson(Person person) {
            int last = personHolderList.size() - 1;
            if (last > -1) {
                if (personHolderList.get(last).isFooter()) {
                    personHolderList.add(last, new PersonHolder(person));
                    return last;
                } else {
                    personHolderList.add(new PersonHolder(person));
                    return last + 1;
                }
            } else {
                personHolderList.add(new PersonHolder(person));
                return 0;
            }
        }

        public int addFooter() {
            int last = personHolderList.size() - 1;
            if (last > -1) {
                if (personHolderList.get(last).isFooter()) {
                    return last;
                }
                personHolderList.add(new PersonHolder(null));
                return last + 1;
            }
            personHolderList.add(new PersonHolder(null));
            return 0;
        }

        public int removeFooter() {
            int last = personHolderList.size() - 1;
            if (last > -1 && personHolderList.get(last).isFooter()) {
                personHolderList.remove(last);
                return last;
            }
            return -1;
        }

        int size() {
            return personHolderList.size();
        }

        Person getPerson(int position) {
            return personHolderList.get(position).getPerson();
        }

        int getHolderType(int position) {
            return personHolderList.get(position).isFooter() ? FOOTER : PERSON_CARD;
        }
    }
}
