package com.personal.ivanmakarychev.networkproximityapp;

import android.app.Application;

import com.vk.sdk.VKSdk;

public class NPApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        VKSdk.initialize(this);
    }
}
