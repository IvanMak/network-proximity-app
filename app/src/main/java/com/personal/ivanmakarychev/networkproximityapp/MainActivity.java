package com.personal.ivanmakarychev.networkproximityapp;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.personal.ivanmakarychev.networkproximityapp.model.Person;
import com.squareup.picasso.Picasso;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    public static final String VK_URL_PREFIX = "https://vk.com/id";
    private static final String TAG = "MainActivity";
    private static final String PREFIX = "npa";
    private static final int REQUEST_ENABLE_BT_DISCOVERABLE = 1;

    private final Pattern USER_ID_PATTERN = Pattern.compile(PREFIX + "\\d+");
    private final AtomicBoolean turningOn = new AtomicBoolean(false);
    private final PersonListAdapter.PersonHolderList personHolderList = new PersonListAdapter.PersonHolderList();
    private final Set<String> alreadyAddedDeviceNameSet = new HashSet<>();

    private ImageView principalImageView;
    private TextView principalNameTextView;
    private LinearLayout principalInfo;
    private RecyclerView personListRecyclerView;
    private Button loginButton;
    private PersonListAdapter personListAdapter;
    private BluetoothAdapter bluetoothAdapter;
    private boolean wasBluetoothOriginallyEnabled;
    private VKAccessToken token;
    private String userId;
    private String oldName;
    private volatile boolean active = false;

    private final VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {

        @Override
        public void onVKAccessTokenChanged(@Nullable VKAccessToken oldToken, @Nullable VKAccessToken newToken) {
            token = newToken;
            if (newToken == null) {
                showErrorAndCleanup("Пожалуйста, авторизуйтесь");
            } else {
                userId = newToken.userId;
                if (bluetoothAdapter != null && bluetoothAdapter.isEnabled()) {
                    bluetoothAdapter.setName(PREFIX + userId);
                }
            }
        }
    };

    private final BroadcastReceiver btReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();
                Log.i(TAG, "Found Bluetooth device " + deviceName);
                if (isValidUserId(deviceName)) {
                    if (!alreadyAddedDeviceNameSet.contains(deviceName)) {
                        alreadyAddedDeviceNameSet.add(deviceName);
                        addPerson(deviceName.substring(3));
                    } else {
                        Log.i(TAG, deviceName + " already added");
                    }
                } else {
                    Log.i(TAG, deviceName + " is not a valid userId");
                }

            }
        }
    };

    private final BroadcastReceiver discoverableReceiver = new ScanModeReceiver();

    private final BroadcastReceiver btStateOnReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int state = intent
                    .getIntExtra(BluetoothAdapter.EXTRA_STATE, -145);
            if (state == BluetoothAdapter.STATE_ON) {
                setDeviceNameAndStartDiscovery(PREFIX + userId);
            } else if (state == BluetoothAdapter.STATE_OFF) {
                Log.i(TAG, "Bluetooth is off");
                removeSearchingIndicator();
            }
        }
    };

    private final BroadcastReceiver btStopDiscoveryReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "Discovery stopped");
            if (active) {
                if (bluetoothAdapter.startDiscovery()) {
                    Log.i(TAG, "Discovery started");
                } else {
                    Log.i(TAG, "Discovery not started");
                    removeSearchingIndicator();
                }
            }
        }
    };

    // region Lifecycle methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupRecyclerView();
        principalInfo = (LinearLayout) findViewById(R.id.principalInfo);
        principalImageView = (ImageView) findViewById(R.id.principalImageView);
        principalNameTextView = (TextView) findViewById(R.id.textViewPrincipalName);
        loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new LoginButtonListener(this));
        initVK();
    }

    @Override
    protected void onResume() {
        super.onResume();
        active = true;
        if (bluetoothAdapter != null) {
            int state = bluetoothAdapter.getState();
            if ((state == BluetoothAdapter.STATE_OFF || state == BluetoothAdapter.STATE_TURNING_OFF)
                    && turningOn.compareAndSet(false, true)) {
                Log.i(TAG, "From onResume: Bluetooth was off");
                enableDiscoverable();
            } else {
                int scanMode = bluetoothAdapter.getScanMode();
                if ((scanMode == BluetoothAdapter.SCAN_MODE_NONE || scanMode == BluetoothAdapter.SCAN_MODE_CONNECTABLE)
                        && turningOn.compareAndSet(false, true)) {
                    Log.i(TAG, "From onResume: discoverable was off");
                    enableDiscoverable();
                }
                setDeviceNameAndStartDiscovery(PREFIX + userId);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        active = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bluetoothAdapter != null) {
            unregisterReceiver(btReceiver);
            unregisterReceiver(discoverableReceiver);
            unregisterReceiver(btStateOnReceiver);
            unregisterReceiver(btStopDiscoveryReceiver);
            bluetoothAdapter.cancelDiscovery();
            if (!wasBluetoothOriginallyEnabled) {
                bluetoothAdapter.disable();
            }
        }
        vkAccessTokenTracker.stopTracking();
        Log.i(TAG, "onDestroy");
    }

    // region Override methods

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(final VKAccessToken res) {
                token = res;
                userId = token.userId;
                loadPrincipalInfo(userId);
                Log.i(TAG, "VK authorized");
                loginButton.setVisibility(View.INVISIBLE);
                personListRecyclerView.setVisibility(View.VISIBLE);
                Log.i(TAG, "User ID = " + userId);
                setUpBluetooth(PREFIX + userId);
            }
            @Override
            public void onError(VKError error) {
                Log.e(TAG, "VK login failed: " + error.errorMessage);
                showErrorAndCleanup("Не удалось авторизоваться. Для продолжения работы необходимо авторизоваться");
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == REQUEST_ENABLE_BT_DISCOVERABLE) {
                if (resultCode == RESULT_CANCELED) {
                    turningOn.set(false);
                    showErrorAndCleanup("Bluetooth должен быть включен для продолжения работы");
                } else {
                    showSearchingIndicator();
                }
            }
        }
    }

    // region Private methods

    private void loadPrincipalInfo(final String principalId) {
        VKRequest rq = VKApi.users()
                .get(VKParameters.from(VKApiConst.USER_IDS, principalId, VKApiConst.FIELDS, "photo_200"));
        rq.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                try {
                    principalInfo.setVisibility(View.VISIBLE);
                    JSONObject personJson = response.json.getJSONArray("response").getJSONObject(0);
                    Log.i(TAG, "Principal: " + personJson.toString());
                    principalNameTextView.setText(personJson.getString("first_name") + " "
                            + personJson.getString("last_name"));
                    Picasso.with(principalImageView.getContext())
                            .load(personJson.getString("photo_200"))
                            .placeholder(R.drawable.loading_indicator)
                            .error(R.drawable.camera_200)
                            .into(principalImageView);
                    principalImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(VK_URL_PREFIX + userId));
                            principalImageView.getContext().startActivity(intent);
                        }
                    });
                } catch (JSONException e) {
                    principalInfo.setVisibility(View.INVISIBLE);
                    Log.e(TAG, "Error in VK response", e);
                }
            }

            @Override
            public void onError(VKError error) {
                principalInfo.setVisibility(View.INVISIBLE);
                Log.e(TAG, "Load principal info from VK failed: " + error.errorMessage);
            }
        });
    }

    private void addPerson(String personId) {
        VKRequest rq = VKApi.users()
                .get(VKParameters.from(VKApiConst.USER_IDS, personId, VKApiConst.FIELDS, "career,photo_200"));
        rq.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                try {
                    JSONObject personJson = response.json.getJSONArray("response").getJSONObject(0);
                    Log.i(TAG, personJson.toString());
                    Person p = new Person();
                    if (p.fromJson(personJson)) {
                        int position = personHolderList.addPerson(p);
                        personListAdapter.notifyItemInserted(position);
                        personListRecyclerView.scrollToPosition(position);
                        Log.i(TAG, "Person " + p.getName() + " added to position " + position);
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "Error in VK response", e);
                }
            }

            @Override
            public void onError(VKError error) {
                Log.e(TAG, "Load from VK failed: " + error.errorMessage);
            }
        });
    }

    private void showSearchingIndicator() {
        int position = personHolderList.addFooter();
        personListAdapter.notifyItemInserted(position);
    }

    private void removeSearchingIndicator() {
        int position = personHolderList.removeFooter();
        if (position != -1) {
            personListAdapter.notifyItemRemoved(position);
        }
    }

    private void setupRecyclerView() {
        personListRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        personListRecyclerView.setHasFixedSize(true);
        personListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        personListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        personListAdapter = new PersonListAdapter(personHolderList);
        personListRecyclerView.setAdapter(personListAdapter);
    }

    private void enableDiscoverable() {
        Intent discoverableIntent =
                new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
        startActivityForResult(discoverableIntent, REQUEST_ENABLE_BT_DISCOVERABLE);
        Log.i(TAG, "enableDiscoverable");
    }

    private void setUpBluetooth(String deviceName) {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Log.i(TAG, "Bluetooth not supported");
            showErrorAndCleanup("Ваш девайс не поддерживает Bluetooth. Дальнейшая работа невозможна");
        } else {
            oldName = bluetoothAdapter.getName();
            registerReceiver(btReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
            registerReceiver(btStateOnReceiver,
                    new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
            registerReceiver(btStopDiscoveryReceiver,
                    new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));
            wasBluetoothOriginallyEnabled = bluetoothAdapter.isEnabled();
            if (wasBluetoothOriginallyEnabled) {
                Log.i(TAG, "Bluetooth already enabled");
                setDeviceNameAndStartDiscovery(deviceName);
            }

            if (turningOn.compareAndSet(false, true)) {
                Log.i(TAG, "From setUpBluetooth: need to enable discoverable");
                enableDiscoverable();
            }
            Log.i(TAG, "Bluetooth set up");
            registerReceiver(discoverableReceiver,
                    new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED));
        }
    }

    private void setDeviceNameAndStartDiscovery(String deviceName) {
        bluetoothAdapter.setName(deviceName);
        Log.i(TAG, "Set device name: " + deviceName);
        if (!bluetoothAdapter.startDiscovery()) {
            Log.i(TAG, "Failed to start discovery");
//            showErrorAndCleanup("Не удалось начать поиск");
        } else {
            Log.i(TAG, "Discovery started");
        }
    }

    private void initVK() {
        vkAccessTokenTracker.startTracking();
        VKSdk.login(this);
    }

    private void showError(String msg) {
        Toast.makeText(getApplicationContext(), msg,
                Toast.LENGTH_LONG).show();
    }

    private void cleanup() {
        if (bluetoothAdapter != null) {
            bluetoothAdapter.setName(oldName);
            if (!wasBluetoothOriginallyEnabled) {
                bluetoothAdapter.disable();
            }
        }
        personListRecyclerView.setVisibility(View.INVISIBLE);
        principalInfo.setVisibility(View.INVISIBLE);
        loginButton.setVisibility(View.VISIBLE);
    }

    private void showErrorAndCleanup(String msg) {
        showError(msg);
        cleanup();
    }

    private boolean isValidUserId(String id) {
        return id != null && USER_ID_PATTERN.matcher(id).matches();
    }

    // region Inner classes

    private static class LoginButtonListener implements Button.OnClickListener {
        private Activity activity;

        LoginButtonListener(Activity activity) {
            this.activity = activity;
        }

        @Override
        public void onClick(View view) {
            VKSdk.login(activity);
        }
    }

    private class ScanModeReceiver extends BroadcastReceiver {
        // на старых девайсах не работает EXTRA_PREVIOUS_SCAN_MODE
        private volatile int previousScanMode = 0;

        @Override
        public void onReceive(Context context, Intent intent) {
            int scanMode = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE,-143);
            if (active && previousScanMode == BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE
                    && turningOn.compareAndSet(false, true)) {
                Log.i(TAG, "Need to enable discoverable");
                enableDiscoverable();
            }
            if (scanMode == BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
                turningOn.set(false);
                Log.i(TAG, "Discoverable enabled");
            }
            previousScanMode = scanMode;
        }
    }
}
