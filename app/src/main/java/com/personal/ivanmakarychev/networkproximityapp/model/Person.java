package com.personal.ivanmakarychev.networkproximityapp.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.personal.ivanmakarychev.networkproximityapp.MainActivity.VK_URL_PREFIX;

public class Person {
    private String login;
    private String name;
    private String job;
    private String imageUrl;
    private String url;

    public boolean fromJson(JSONObject json) {
        try {
            login = json.getString("id");
            name = json.getString("first_name") + " " + json.getString("last_name");
            imageUrl = json.getString("photo_200");
            url = VK_URL_PREFIX + login;

            JSONArray career = json.optJSONArray("career");
            if (career != null) {
                JSONObject job;
                for (int i = 0; i < career.length(); i++) {
                    job = career.getJSONObject(i);
                    if (!job.has("until")) {
                        String comp = job.optString("company");
                        String position = job.optString("position");
                        String delimiter = !comp.isEmpty() && !position.isEmpty() ? " - " : "";
                        this.job = position + delimiter + comp;
                        break;
                    }
                }
            }

            return true;

        } catch (JSONException e) {
            Log.e("Person", "JSON error", e);
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (this == o) return true;

        if (o instanceof Person) {
            Person person = (Person) o;
            return name.equals(person.name)
                    && login.equals(person.login)
                    && job.equals(person.job)
                    && imageUrl.equals(person.imageUrl)
                    && url.equals(person.url);
        }

        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
