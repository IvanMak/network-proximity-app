# Network Proximity App

Network Proximity App implements an alternative approach to social media 
interaction with user location.

## More information

Please see the article http://injoit.org/index.php/j1/article/view/887

## How to contribute

Your contribution is welcome. Please make a pull request.
